using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CharacterMovement : MonoBehaviour
{
    public float speed = 5f;
    public float jumpheight = 2f;
    [Range(0.01f, 1f)]
    public float forwardjumpfactor = 0.05f;
    public float Gravity = -9.8f;
    public float DashFactor = 2f;
    public Vector3 Drag = new Vector3(1f, 2f, 1f);
    public float smoothTime = 0.15f;

    private CharacterController characterController;
    private Vector3 movedirection;
    private Vector3 Smoothmovedirection;
    private Vector3 Smoother;
    private Vector3 horizontalVelocity;

    public bool isGrounded { get { return characterController.isGrounded; } }
    public float currentSpeed { get { return horizontalVelocity.magnitude; } }
    public float currentNormalizedSpeed { get { return horizontalVelocity.normalized.magnitude; } }

    
    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();    
    }


    public void moveCharacter(float hInput, float vInput, bool jump, bool dash)
    {
        float DeltaTime = Time.deltaTime;
        float dashF = 1f;

        if (characterController.isGrounded)
        {
            movedirection = (hInput * transform.right + vInput * transform.forward).normalized;

            if (dash) dashF = DashFactor;
            if (jump)
            {
                if(Mathf.Abs(movedirection.x) > 0f || Mathf.Abs(movedirection.z) > 0f)
                {
                    movedirection += movedirection.normalized * (Mathf.Sqrt(jumpheight * forwardjumpfactor * -Gravity / 2) * dashF);
                }

                movedirection.y = Mathf.Sqrt(jumpheight * -2f * Gravity);
            } 
        
        }

        //gravedad
        movedirection.y += Gravity * DeltaTime;

        //Drag
        movedirection.x /= 1 + Drag.x * DeltaTime;
        movedirection.y /= 1 + Drag.y * DeltaTime;
        movedirection.z /= 1 + Drag.z * DeltaTime;

        //Smooth direction
        Smoothmovedirection = Vector3.SmoothDamp(Smoothmovedirection, movedirection, ref Smoother, smoothTime);

        //jump is not smoothed
        Smoothmovedirection.y = movedirection.y;

        //apply move to character
        characterController.Move(movedirection * (DeltaTime * speed * dashF));

        //cache horizontal speed
        horizontalVelocity.Set(characterController.velocity.x, 0, characterController.velocity.z);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
