using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{

    [SerializeField]
    private Vector3 RotationAxes;
    public float RotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        RotationAxes = Movement.ClampVector3(RotationAxes);
        transform.Rotate(RotationAxes * (RotationSpeed * Time.deltaTime));
    }
}
