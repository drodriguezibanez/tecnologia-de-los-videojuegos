using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoasntroller : MonoBehaviour
{
    private Rigidbody rigidbody;
    private Animator animator;
    private float hInput;
    private float vInput;
    private float runInput;
    private float currentSpeed = 0;
    public float walkSpeed = 3;
    public float runSpeed = 6;
    public float turnSmoothing = 20f;

    public float idlewaitingTime = 5f;
    public float currentWaitingTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        
        if(rigidbody == null)
        {
            Debug.Log("No se ha encontrado ning�n rigidbody en este componente");
            enabled = false;
            return;
        }

        animator = GetComponent<Animator>();

        if(animator == null)
        {
            Debug.Log("no se ha encontrado ninguna animaci�n para este componente");
            enabled = false;
            return;
        }
    }

    // Update is called once per frame
    void Update()
    {
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");
        runInput = Input.GetAxis("Dash");

        handleRotation();

        if(currentSpeed == 0)
        {
            currentWaitingTime += Time.deltaTime;
        }
        else
        {
            currentWaitingTime = 0;
        }
    }

    private void OnAnimatorMove()
    {
        animator.SetFloat("Speed", currentSpeed);

        if(currentWaitingTime >= idlewaitingTime)
        {
            if (Random.Range(0, 99) < 30)
            {
                animator.SetTrigger("Idle wait");
            }

            currentWaitingTime = 0;
        }
       
    }

    private void FixedUpdate()
    {

        handleMovement();
    }


    private void handleMovement()
    {
        float targetSpeed = walkSpeed;
        if (runInput > 0)
        {
            targetSpeed = runSpeed;
        }

        if (hInput != 0 || vInput != 0)
        {
            rigidbody.velocity = new Vector3(hInput, 0, vInput) * targetSpeed;

        }
        else
        {
            rigidbody.velocity = Vector3.zero; //para al jugador

        }

        currentSpeed = rigidbody.velocity.magnitude; //convierte velocity en Speed

    }

    private void handleRotation()
    {
        if (hInput != 0 || vInput != 0)
        {
            Quaternion newROtation = Quaternion.LookRotation(new Vector3(hInput, 0, vInput));
            transform.rotation = Quaternion.Slerp(transform.rotation, newROtation, Time.deltaTime * turnSmoothing);
        }
    }
}
