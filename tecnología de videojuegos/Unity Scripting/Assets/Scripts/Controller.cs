using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public float Speed = 4f;
    public float angularSpeed = 30f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        {

            transform.Rotate(Vector3.up * (-angularSpeed * Time.deltaTime));

        } else if(Input.GetKey(KeyCode.D))
        {

            transform.Rotate(Vector3.up * (angularSpeed * Time.deltaTime));

        }

        if (Input.GetKey(KeyCode.W))
        {
            //he a�adido cambios ya que los ejes est�n de otra manera en mi proyecto de unity, por lo que en vez de moverse hacia delante o atr�s se mov�a hacia los lados
            transform.Translate(Vector3.forward * (Time.deltaTime * Speed));
            //                         (forward)
        }
        else if (Input.GetKey(KeyCode.S))
        {
            //he a�adido cambios ya que los ejes est�n de otra manera en mi proyecto de unity, por lo que en vez de moverse hacia delante o atr�s se mov�a hacia los lados
            transform.Translate(Vector3.back * (Time.deltaTime * Speed));
            //                         (back)
        }

    }
}
